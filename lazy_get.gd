extends Node


# Sometimes a child node needs to be accessed before the node is added to the
# scene tree, and @onready vars aren't initialized in time (since the `ready`
# lifecycle phase hasn't run yet). In these situations the lazy getter pattern
# comes in handy. It's a replacement for any @onready var which:
# 1. is set only once and never changes
# 2. needs to be accessed before the `ready` lifecycle phase
# 3. is not known beforehand (so cannot be set in the editor or as a const)
# It's a shame the language doesn't have a @lazy annotation but
# this will do the job.

var lazy_foo: Node
var foo: Node:
	get:
		if lazy_foo == null:
			lazy_foo = $Whatever
		return lazy_foo
