extends Node

# Sometimes you need to make sure a certain value is true, non-null, etc. before
# proceeding, and it not being set just means you need to wait a bit. This might
# happen if two _ready() call backs don't occur in the order you want, etc.
#
# One solution is to define a signal to be emitted whenever that variable is set.

signal player_loaded

var player: CharacterBody2D:
	set(value):
		player = value
		if player != null:
			player_loaded.emit()


# ...

func play_player_entry_animation():
	if player == null:
		await player_loaded

	player.get_node("AnimationPlayer").play("greeting")
